# HW1
# the client's sent over a new sign-up with username and password

# validate that the email only contains alphanumeric characters.

form = { :firstname => "Jimmy", :lastname => "Qiu", :email => "validemail@mail.com",
         :username => "suparoboto", :password => "something0K!" }

# Check if email is valid
#
# email - email string
#
# Return true if email is valid
def validate_email(email)
  # TODO check that the email has valid local and global past

  # Uppercase and lowercase English letters (a-z, A-Z)
  # Digits 0 to 9
  # Character . provided that it is not the first or last character

end

# Validate that the password strength is adequate
#
# password - the password string
#
# Return strength of password
def password_strength(password)
  # TODO check strength of password
  status = :weak

  # letters
  # numbers
  # symbols in !@#$%^&*()

  # :weak if only satisfy 1 of the above
  # :medium if satisfy 2 of the checks
  # :strong if statisfy 3 of the checks
end

# HW2
# on the server side, you've retrieved from the database a set of tweets from a user
# we need to turn it into json string for clients to consume

user = { :username => "suparoboto", :tweets => ["Hello world", "For elegant form, always toss with your main hand"] }

# json string:
# "{
#   "username": "suparoboto",
#   "tweets": ["Hello world", "For elegant form, always toss with your main hand"]
# }"

# Construct json string from user hash
#
# user - user hash retrieved from db
#
# Return json string
def construct_json(user)
  user_json = ""

  # TODO construct valid json string using user hash
  #      check validity using: http://jsonlint.com/
end
